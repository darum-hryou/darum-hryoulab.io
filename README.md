
[![주식회사 다름](https://gitlab.com/uploads/-/system/project/avatar/5585034/darum_logo.png)](http://darum-iyjeong.gitlab.io)

## 프로젝트 관리


### 1. 다름 홈페이지 개편

  - 일정 : [WBS 일정 관리 확인](https://docs.google.com/spreadsheets/d/1uUSsapprOolB5cSK_QAL_5FFxmOtLsq8D_soyjl7gSI/edit?usp=sharing)
  - 데모 : [darum/index.html](http://darum-iyjeong.gitlab.io/darum/index.html)

### 2. 피자알볼로 홈페이지 구축
  - 일정 : [WBS 일정 관리 확인](https://docs.google.com/spreadsheets/d/1tq9226W-ai1ilUtDJ4W5pElju1ZLIUcCY4qyn0Er9GM/edit?usp=sharing)
  - 데모 : [pizzaalvolo/index.html](http://darum-iyjeong.gitlab.io/pizzaalvolo/index.html)


## 퍼블리싱팀 일정 관리

  - [PUB] 프로젝트 캘린더 : [업무 관련 관리](https://calendar.google.com/calendar?cid=ZGFydW0uaXlqZW9uZ0BnbWFpbC5jb20)
  - [DARUM] 팀 공지 캘린더 : [이슈 공유 관리](https://calendar.google.com/calendar?cid=ZGFydW0uaXlqZW9uZ0BnbWFpbC5jb20)
